/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Timo Bittner - hive GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

function echo_window_load () {
    $(window).load(function(){
        if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
            console.info('jquery echo initialize');
        }
        echo.init({
            offset: 250,
            offsetVertical: 250,
            throttle: 250,
            unload: false,
            callback: function (element, op) {
                if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                    console.log(element, 'has been', op + 'ed');
                }
                if(op === 'load') {
                    //element.classList.add('opacity_0');
                    //element.classList.add('fadeIn');
                    element.className += ' fadeIn';
                }
            }
        });
        echo.render();
    });

    $(window).trigger('load');
}

var hive_thm_jq_echo__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        clearInterval(hive_thm_jq_echo__interval);

        if (typeof hive_thm_jq__load_concatenate == 'undefined' || !hive_thm_jq__load_concatenate) {
            loadScript("/typo3conf/ext/hive_thm_jq_echo/Resources/Public/Assets/Js/echo.min.js.gzip?v=1", function () {
                if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                    console.info('jquery echo loaded');
                }

                echo_window_load();

            });
        } else {
            echo_window_load();
        }


    }

}, 2000);

