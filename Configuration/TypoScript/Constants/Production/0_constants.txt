plugin.tx_hive_thm_jq_echo {
    settings {
        production {
            includePath {
                public = EXT:hive_thm_jq_echo/Resources/Public/
                private = EXT:hive_thm_jq_echo/Resources/Private/
                frontend {
                    public = typo3conf/ext/hive_thm_jq_echo/Resources/Public/
                }
            }
            optional {
            	active = 0
            }
        }
    }
}